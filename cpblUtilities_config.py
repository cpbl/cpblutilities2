#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, re, sys, copy
from configtools import read_hierarchy_of_config_files,readConfigFile

"""This module provides dicts paths and defaults, which contain any parameters needed by multiple other modules.
These parameters are generally unchanging over time, but may vary from one installation/environment to another.

There are four places these config settings could be set. In order of priority:

(1) local config.cfg file
(2) local config-template.cfg file
(3) cpblUtilities source folder config.cfg file
(4) cpblUtilities source folder config-template.cfg file


 - Specify structure of file.
 - Load cascaded values from config files.
 - Then rearrange as we need to put them into the dict arrays paths and defaults.

Note that some key functions located in other modules of cpblUtilities are reproduced
here, in order to avoid circular dependencies. e.g. dsetset, dgetget,
and even a version of read_hierarchy_of_config_files()

"""
UTILS_config_file_structure={
    'paths': [
        'working',
        'input',
        'graphics',
        'outputdata',
        'output',
        'tex',
        'scratch',
        'bin',
        'svgmaptemplates',
        ],
    'defaults': [
        ('rdc',bool),
        'mode',
        ],
    }



def update_paths(defs): #copied from pystata
    """ Utility to copy some key output paths from another module's configuration. This is not normally needed, since below we look in the local folder for a config file, but because other modules may update their paths (e.g. osm's  update_paths_to_be_specific_to_database), this may be necessary to update things again. 
 This method refers to a global defaults, defined if this method is imported into cpblUtilities' core
"""
    if 'paths' in defs: defs=defs['paths']
    # Instead of the following, simply use defaults['paths'].update(defs) ????
    for folder in ['scratch','working','output','tex']:
        if folder in defs:
            print(' cpblUtils updating {}={} to {}'.format(folder, defaults['paths'][folder], defs[folder]))
            defaults['paths'][folder] = defs[folder]
    return defaults


def _notgeneral_readConfigFile(inpath):
    import ConfigParser
    config = ConfigParser.SafeConfigParser({'pwd': os.getcwd(),'cwd': os.getcwd()})
    config.read(inpath)
    outdict={}
    for section in     config_file_structure:
        if config.has_section(section):
            for option in config_file_structure[section]:
                if config.has_option(section,option  if isinstance(option,str) else option[0]):
                    if isinstance(option,str):
                        dsetset(outdict,(section,option), config.get(section,option))
                    elif option[1]==bool:
                        dsetset(outdict,(section,option[0]), config.getboolean(section,option[0]))
                    elif option[1]==int:
                        dsetset(outdict,(section,option[0]), config.getint(section,option[0]))
                    elif option[1]=='commasep':
                        dsetset(outdict,(section,option[0]), config.get(section,option[0]).split(','))
    return(outdict)


def _co_read_hierarchy_of_config_files(files):
    """
    There is a more general version of this in configtools, which is used by other modules. But I can't use that one because these modules would be cross-dependent
    
    """
    configDict={}
    for ff in files:
        if os.path.exists(ff):
            newConfigDict=readConfigFile(ff)
            configDict=merge_dictionaries(configDict,newConfigDict, verboseSource=False) #bool(configDict))


    if not configDict:
        raise Exception("Cannot find config[-template].cfg file in "+', '.join(files))
    return configDict



def main():
    """
    """
    localConfigFile=os.getcwd()+'/config.cfg'
    localConfigTemplateFile=os.getcwd()+'/config-template.cfg'
    repoPath=os.path.abspath(os.path.dirname(__file__ if __file__ is not None else '.'))

    if 0: 
        # Change directory to the bin folder, ie location of this module. That way, we always have the config.cfg file as local, which means other utlilities using config.cfg will find the right one.
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path)
        if 'cpblUtilities' not in os.getcwd():
            os.chdir(dir_path)

    repoFile=(repoPath if repoPath else '.')+'/config.cfg'
    repoTemplateFile=(repoPath if repoPath else '.')+'/config-template.cfg'


    print('cpblUtilities setting defaults:')
    merged_dictionary=read_hierarchy_of_config_files([
        repoTemplateFile,
        repoFile,
        localConfigTemplateFile,
        localConfigFile,
    ], UTILS_config_file_structure)

    # Now impose our structure
    defaults=dict([[kk,vv] for kk,vv in merged_dictionary.items() if kk in ['rdc','mode']])
    defaults.update(dict(paths=merged_dictionary['paths'],
                  ))
    defaults['stata']={'paths':copy.deepcopy(defaults['paths'])}
    return(defaults)




defaults=main()
paths=defaults['paths']
if 'python_utils_path' in paths:
    sys.path.append(paths['python_utils_path'])

