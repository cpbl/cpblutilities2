#!/usr/bin/python

# Actually, I think each method in mathgraph.py should have a test_method() in mathgraph.py. This module here should simply call all those tests.
from cpblUtilities.mathgraph import *


df = pd.DataFrame({"A": [10, 3,4,5,6,7,20, 30, 324, 2353, np.nan],
                   "B": [20, 4,3,6,5,7.1,30, 10, 100, 2332, 2332],
                   'w':[14, 10,10,10,10,10,    14,20,14,14,14,],
                    })

if 0: 
    df1=df.dropna()
    plot(df1.A,df1.B,'.')
    dfOverplotLinFit(df1,'A','B',label=None)#
if 0: 
    import seaborn as sns
    tips = sns.load_dataset("tips")
    dfOverplotLinFit(tips, 'total_bill','tip',label=None)#


test_dfOverplotLinFit()


test_savefigall() 
